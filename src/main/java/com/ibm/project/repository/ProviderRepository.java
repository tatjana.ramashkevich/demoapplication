package com.ibm.project.repository;

import com.ibm.project.domain.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long> {

  List<Provider> findAll();

  List<Provider> findByIdCustomer(Long idCustomer);
}
