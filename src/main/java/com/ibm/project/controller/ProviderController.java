package com.ibm.project.controller;

import com.ibm.project.dto.ProviderDto;
import com.ibm.project.service.DefaultProviderService;
import lombok.AllArgsConstructor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
@AllArgsConstructor
public class ProviderController {
  private final DefaultProviderService defaultProviderService;

  @GetMapping("/providers")
  List<ProviderDto> all() {
    return defaultProviderService.listAllProviders();
  }

  @GetMapping("/providers/{idCustomer}")
  List<ProviderDto> getByCustomerId(@PathVariable Long idCustomer) {
    return defaultProviderService.getProvidersByCustomerId(idCustomer);
  }

}
