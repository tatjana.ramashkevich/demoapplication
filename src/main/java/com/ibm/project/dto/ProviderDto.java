package com.ibm.project.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ProviderDto {
  private Long id;

  private String name;

  private LocalDateTime registrationDate;

  private Long idCustomer;
}
