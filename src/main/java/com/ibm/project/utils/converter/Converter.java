package com.ibm.project.utils.converter;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public abstract class Converter<T, U> {

  public abstract U convert(T data);

  public List<U> convertList(List<T> data) {
    if (data == null) {
      return new ArrayList<>();
    }
      return data.stream()
                .map(this::convert)
                .toList();
  }

}
