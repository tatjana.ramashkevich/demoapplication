package com.ibm.project.utils.converter.provider;

import com.ibm.project.domain.Provider;
import com.ibm.project.dto.ProviderDto;
import com.ibm.project.utils.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProviderDtoToProviderConverter extends Converter<ProviderDto, Provider> {
  @Override
  public Provider convert(ProviderDto providerDto) {
    return Provider.builder()
                .id(providerDto.getId())
                .name(providerDto.getName())
                .registrationDate(providerDto.getRegistrationDate())
                .idCustomer(providerDto.getIdCustomer())
                .build();
  }

}
