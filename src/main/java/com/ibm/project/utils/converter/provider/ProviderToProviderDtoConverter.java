package com.ibm.project.utils.converter.provider;

import com.ibm.project.domain.Provider;
import com.ibm.project.dto.ProviderDto;
import com.ibm.project.utils.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ProviderToProviderDtoConverter extends Converter<Provider, ProviderDto> {
  @Override
  public ProviderDto convert(Provider provider) {
    return ProviderDto.builder()
                .id(provider.getId())
                .name(provider.getName())
                .registrationDate(provider.getRegistrationDate())
                .idCustomer(provider.getIdCustomer())
                .build();
  }
}
