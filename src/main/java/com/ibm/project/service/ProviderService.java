package com.ibm.project.service;

import com.ibm.project.dto.ProviderDto;

import java.util.List;

public interface ProviderService {
  List<ProviderDto> listAllProviders();
  List<ProviderDto> getProvidersByCustomerId(Long idCustomer);
}
