package com.ibm.project.service;

import com.ibm.project.dto.ProviderDto;
import com.ibm.project.repository.ProviderRepository;
import com.ibm.project.utils.converter.provider.ProviderToProviderDtoConverter;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DefaultProviderService implements ProviderService {
  private final ProviderRepository providerRepository;

  private final ProviderToProviderDtoConverter providerToProviderDtoConverter;

  public List<ProviderDto> listAllProviders() {
    return providerToProviderDtoConverter.convertList(providerRepository.findAll());
  }

  public List<ProviderDto> getProvidersByCustomerId(Long idCustomer) {
    return providerToProviderDtoConverter.convertList(providerRepository.findByIdCustomer(idCustomer));
  }

}
