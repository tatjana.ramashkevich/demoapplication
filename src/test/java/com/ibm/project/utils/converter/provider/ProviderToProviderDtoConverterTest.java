package com.ibm.project.utils.converter.provider;

import com.ibm.project.domain.Provider;
import com.ibm.project.dto.ProviderDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ProviderToProviderDtoConverterTest {

  @InjectMocks
  ProviderToProviderDtoConverter providerToProviderDtoConverter;

  private static Provider provider;

  @BeforeAll
  static void setUp() {
    provider = Provider.builder()
            .id(1L)
            .name("Test name")
            .registrationDate(LocalDateTime.now())
            .idCustomer(3L)
            .build();
  }

  @Test
  void convertProviderToProviderDto() {
    ProviderDto providerDto = providerToProviderDtoConverter.convert(provider);

    assertEquals(provider.getId(), providerDto.getId());
    assertEquals(provider.getName(), providerDto.getName());
    assertEquals(provider.getRegistrationDate(), providerDto.getRegistrationDate());
    assertEquals(provider.getIdCustomer(), providerDto.getIdCustomer());
  }
}
