package com.ibm.project.utils.converter.provider;

import com.ibm.project.domain.Provider;
import com.ibm.project.dto.ProviderDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ProviderDtoToProviderConverterTest {

  @InjectMocks
  ProviderDtoToProviderConverter providerDtoToProviderConverter;

  private static ProviderDto providerDto;

  @BeforeAll
  static void setUp() {
    providerDto = ProviderDto.builder()
            .id(1L)
            .name("Test name")
            .registrationDate(LocalDateTime.now())
            .idCustomer(6L)
            .build();
  }

  @Test
  void convertProviderDtoToProvider() {
    Provider provider = providerDtoToProviderConverter.convert(providerDto);

    assertEquals(providerDto.getId(), provider.getId());
    assertEquals(providerDto.getName(), provider.getName());
    assertEquals(providerDto.getRegistrationDate(), provider.getRegistrationDate());
    assertEquals(providerDto.getIdCustomer(), provider.getIdCustomer());
  }
}
