package com.ibm.project.utils.converter;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ConverterTest {

  Converter<String, String> converter = new Converter<String, String>() {
      @Override
      public String convert(String data) {
          return data.toLowerCase();
      }
  };

  @Test
  void convertNull() {
    List<String> result = converter.convertList(null);

    assertNotNull(result);
    assertTrue(result.isEmpty());
  }

  @Test
  void convertListOfStrings() {
    String value = "Test value";
    List<String> result = converter.convertList(List.of(value));

    assertEquals(1, result.size());
    assertEquals(value.toLowerCase(Locale.ROOT), result.get(0));
  }
}
