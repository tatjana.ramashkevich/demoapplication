package com.ibm.project.controller;

import com.ibm.project.dto.ProviderDto;
import com.ibm.project.service.DefaultProviderService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class ProviderControllerTest {

  @Autowired
  MockMvc mockMvc;
  @MockBean
  DefaultProviderService defaultProviderService;
  private static final List<ProviderDto> providerDtoList = new ArrayList<>();

  @BeforeAll
  static void setup() {
    ProviderDto providerDto1 = ProviderDto.builder()
        .id(1L)
        .name("test name1")
        .idCustomer(5L)
        .build();
    ProviderDto providerDto2 = ProviderDto.builder()
        .id(2L)
        .name("test name2")
        .idCustomer(6L)
        .build();
    providerDtoList.addAll(List.of(providerDto1, providerDto2));

  }


  @Test
  void checkAllProvidersReturned() throws Exception {
    when(defaultProviderService.listAllProviders()).thenReturn(providerDtoList);

    mockMvc.perform(get("/providers"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("test name1")))
        .andExpect(content().string(containsString("test name2")));
  }

  @Test
  void checkProvidersReturnedByCustomerId() throws Exception {
    ProviderDto providerDto = providerDtoList.get(0);

    when(defaultProviderService.getProvidersByCustomerId(providerDto.getIdCustomer())).thenReturn(List.of(providerDto));

    mockMvc.perform(get("/providers/" + providerDto.getIdCustomer()))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(providerDto.getName())));
  }

  @Test
  void checkReceivedEmptyListIfCustomerIdNotExist() throws Exception {
    Long customerId = 5L;
    when(defaultProviderService.getProvidersByCustomerId(customerId)).thenReturn(List.of());

    mockMvc.perform(get("/providers/" + customerId))
       .andDo(print())
       .andExpect(status().isOk())
       .andExpect(content().string("[]"));
    }
}
