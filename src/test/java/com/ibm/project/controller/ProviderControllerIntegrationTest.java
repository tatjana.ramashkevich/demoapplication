package com.ibm.project.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class ProviderControllerIntegrationTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  void getAllProviders() throws Exception {
    mockMvc.perform(get("/providers"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Coca-cola")))
        .andExpect(content().string(containsString("Pepsi")))
        .andExpect(content().string(containsString("Redbull")))
        .andExpect(content().string(containsString("Fanta")))
        .andExpect(content().string(containsString("Casera")))
        .andExpect(content().string(containsString("Trina")));

    }

  @Test
  void getProvidersBuCustomerId() throws Exception {
    long customerId = 5L;
    mockMvc.perform(get("/providers/" + customerId))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString("Coca-cola")))
        .andExpect(content().string(containsString("Pepsi")));

  }

  @Test
  void checkReceivedEmptyListIfCustomerIdNotExist() throws Exception {
    Long customerId = 10L;

    mockMvc.perform(get("/providers/" + customerId))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("[]"));
    }
}
