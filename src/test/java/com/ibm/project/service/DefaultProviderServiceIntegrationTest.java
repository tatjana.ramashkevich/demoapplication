package com.ibm.project.service;

import com.ibm.project.dto.ProviderDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DefaultProviderServiceIntegrationTest {

  @Autowired
  private DefaultProviderService defaultProviderService;

  @Test
  void getAllProviders() {
    List<ProviderDto> providerDtos = defaultProviderService.listAllProviders();
    assertEquals(6, providerDtos.size());
  }

  @Test
  void getProvidersByIdCustomer() {
    Long idCustomer = 5L;
    List<ProviderDto> providerDtos = defaultProviderService.getProvidersByCustomerId(idCustomer);

    assertEquals(2, providerDtos.size());
  }

  @Test
  void getEmptyProviderListByIdCustomer() {
    Long idCustomer = 0L;
    List<ProviderDto> providerDtos = defaultProviderService.getProvidersByCustomerId(idCustomer);

    assertEquals(0, providerDtos.size());
  }
}
