package com.ibm.project.service;

import com.ibm.project.domain.Provider;
import com.ibm.project.dto.ProviderDto;
import com.ibm.project.repository.ProviderRepository;
import com.ibm.project.utils.converter.provider.ProviderToProviderDtoConverter;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class DefaultProviderServiceTest {

  @InjectMocks
  DefaultProviderService defaultProviderService;

  @Mock
  ProviderToProviderDtoConverter providerToProviderDtoConverter;

  @Mock
  ProviderRepository providerRepository;

  private static final List<ProviderDto> providerDtoList = new ArrayList<>();

  private static final List<Provider> providerList = new ArrayList<>();

  @BeforeAll
  static void setup() {
    ProviderDto providerDto1 = ProviderDto.builder()
                .id(1L)
                .name("test name1")
                .idCustomer(5L)
                .build();
    ProviderDto providerDto2 = ProviderDto.builder()
                .id(2L)
                .name("test name2")
                .idCustomer(6L)
                .build();
    providerDtoList.addAll(List.of(providerDto1, providerDto2));

    Provider provider1 = Provider.builder()
                .id(1L)
                .name("test name1")
                .idCustomer(5L)
                .build();
    Provider provider2 = Provider.builder()
                .id(2L)
                .name("test name2")
                .idCustomer(6L)
                .build();
    providerDtoList.addAll(List.of(providerDto1, providerDto2));
    providerList.addAll(List.of(provider1, provider2));
  }

  @Test
  void checkReturnAllProviders() {
    when(providerRepository.findAll()).thenReturn(providerList);
    when(providerToProviderDtoConverter.convertList(providerList)).thenReturn(providerDtoList);

    List<ProviderDto> receivedProviders = defaultProviderService.listAllProviders();
    assertEquals(providerDtoList, receivedProviders);
    verify(providerRepository).findAll();
  }

  @Test
  void checkProvidersReturnedByCustomerId() {
    Provider provider = providerList.get(0);
    ProviderDto providerDto = providerDtoList.get(0);
    Long idCustomer = provider.getIdCustomer();

    when(providerRepository.findByIdCustomer(idCustomer)).thenReturn(List.of(provider));
    when(providerToProviderDtoConverter.convertList(List.of(provider)))
                .thenReturn(List.of(providerDto));

    List<ProviderDto> receivedProviderDtos = defaultProviderService.getProvidersByCustomerId(idCustomer);
    assertEquals(List.of(providerDto), receivedProviderDtos);
    verify(providerRepository).findByIdCustomer(idCustomer);

  }
}
