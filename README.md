How to run the program:
1. Download the project from gitlab
2. Open the project within IDE(IntelliJ)
3. Launch the project.
4. Open postman collection. 
5. Run the following calls from the collection.

Dependencies used by the project:
org.junit.jupiter
org.hibernate.validator 6.0.10.Final
spring-boot-starter-data-jpa
spring-boot-starter-web
com.h2database
org.projectlombok
spring-boot-starter-test 